angular.module("numera", ['ngRoute'])
        .config(function ($routeProvider) {

            $routeProvider.when('/Home', {
                templateUrl: "Pages/Listardocs.html",
                controller: 'listaCtrl'
            });
            
            
             $routeProvider.when('/Sobre', {
                templateUrl: "Pages/Sobre.html",
                controller: 'sobreCtrl'
            });

            $routeProvider.otherwise({redirectTo: '/'});
            
        });

    