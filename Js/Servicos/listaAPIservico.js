angular
        .module("numera")
        .factory("listaApi", function ($http, config){
    
    var _listaDocumentos = function (){
        
        return  $http.get( config.baseUrl + "/Read.php");
        
    };
    
    
    return {
        
        getListagem : _listaDocumentos
        
    };
        
});